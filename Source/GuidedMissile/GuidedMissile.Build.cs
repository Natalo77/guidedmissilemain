// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GuidedMissile : ModuleRules
{
	public GuidedMissile(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
