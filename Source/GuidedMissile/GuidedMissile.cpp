// Copyright Epic Games, Inc. All Rights Reserved.

#include "GuidedMissile.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GuidedMissile, "GuidedMissile" );
 