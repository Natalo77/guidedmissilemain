
#include "GuidedMissile/Missile/BasicMissile/BasicMissile.h"

#include <GameFramework/ProjectileMovementComponent.h>

#include <Kismet/KismetMathLibrary.h>
#include <Kismet/GameplayStatics.h>

ABasicMissile::ABasicMissile()
{
	// Create missile body and use as root
	MissileBodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MissileBody"));
	check(MissileBodyMesh);
	SetRootComponent(MissileBodyMesh);

	// Create missile fins and attach to body
	MissileFinMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MissileFin1"));
	MissileFinMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MissileFin2"));
	MissileFinMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MissileFin3"));
	MissileFinMesh4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MissileFin4"));
	check(MissileFinMesh1);
	check(MissileFinMesh2);
	check(MissileFinMesh3);
	check(MissileFinMesh4);
	MissileFinMesh1->SetupAttachment(GetRootComponent());
	MissileFinMesh2->SetupAttachment(GetRootComponent());
	MissileFinMesh3->SetupAttachment(GetRootComponent());
	MissileFinMesh4->SetupAttachment(GetRootComponent());

	// Create movement component - We don't want to auto activate it, only when we've fired it.
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	check(ProjectileMovementComponent);
	ProjectileMovementComponent->SetAutoActivate(false);
}

void ABasicMissile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const EBasicMissileState CurrentStateTemp = GetState();
	switch (CurrentStateTemp)
	{
	case EBasicMissileState::EBMS_Homing:
	{
		TickThrusterAcceleration(DeltaTime);
		TickGravityScale(DeltaTime);
		TickHomingMissileMovement(DeltaTime);
	}
	default:
		; // Don't need to do anything for states we're not concerned about on tick, and a switch is more future proof than an if. I want to pick and choose the states within which I change gravity, so that I can keep my determinstic behaviour.
	}
}

void ABasicMissile::PrepareMissileForLaunch(const FTransform& LaunchTransform)
{
	// Turn off collision to prevent any problems with object intersection
	SetCollisionEnabled(false);

	// Move the actor to the specified transform
	SetActorTransform(LaunchTransform);

	// Move the missile to PreFire state
	SetState(EBasicMissileState::EBMS_PreFire);

	Super::PrepareMissileForLaunch(LaunchTransform);
}

void ABasicMissile::FireMissile()
{
	// Give initial first impulse, then wait for a duration, and move to homing stage.
	if (!ProjectileMovementComponent)
	{
		return;
	}

	SetState(EBasicMissileState::EBMS_InitialLaunch);

	ProjectileMovementComponent->SetActive(true);
	const FVector InitialLaunchForce = GetActorForwardVector() * GetInitialLaunchForceSize();
	ProjectileMovementComponent->AddForce(InitialLaunchForce);

	// Start the timer for moving from initial launch to homing
	const FTimerDelegate TimerDelegate_BeginHomingAfterInitialLaunch = FTimerDelegate::CreateUFunction(this, "StartHoming");
	GetWorldTimerManager().SetTimer(TimerHandle_BeginHomingAfterInitialLaunch, TimerDelegate_BeginHomingAfterInitialLaunch, GetInitialLaunchStageDuration(), false);

	Super::FireMissile();
}

void ABasicMissile::ExplodeMissile()
{
	if (!MissileBodyMesh)
	{
		return;
	}

	const FVector MissileWarheadLocation = MissileBodyMesh->GetSocketLocation("LockingCone");
	const float MissileDamageTemp = GetMissileDamage();
	const float MissileDamageRangeTemp = GetMissileDamageRange();
	const TArray<AActor*> IgnoreActors = { this };
	AActor* DamageCauser = this;
	AController* MissileInstigatorControllerTemp = GetMissileInstigatorController();
	const bool bDoFullDamage = true;

	// This should be set up with its own damage type.
	UGameplayStatics::ApplyRadialDamage(this, MissileDamageTemp, MissileWarheadLocation, MissileDamageRangeTemp, UDamageType::StaticClass(), IgnoreActors, DamageCauser, MissileInstigatorControllerTemp, bDoFullDamage);

	Super::ExplodeMissile();
}

void ABasicMissile::StartHoming()
{
	SetState(EBasicMissileState::EBMS_Homing);

	SetTargetEngineThrustPercentage(1.f);

	// I could move all the behaviour for missile homing to a derivative projectile movement component to prevent tick checking (and then just activate the component here), but in the end that would just be moving the tick check from here to the proposed component.
	// 
	// This function also begins thruster ramp up (instead of just having the thruster's force be binary)
	// I could do this as a timer, but I don't really want the performance of going through all the function calls, and making it a tick check allows me to re-use the functionality later to do other missile effects if I want.
}

void ABasicMissile::SetCollisionEnabled(const bool bEnabled)
{
	if (MissileBodyMesh)
	{
		const ECollisionEnabled::Type MissileBodyCollision = bEnabled ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision;
		MissileBodyMesh->SetCollisionEnabled(MissileBodyCollision);
	}

	// We never want the fins to have collision, so we don't need to worry about them, and shouldn't waste processing disabling them all the time.
}

bool ABasicMissile::IsTargetInLockingCone()
{
	if (!GetTargetActor())
	{
		return false;
	}

	// Cache TargetLocation, MissileConeLocation, Delta, and DeltaSizeSqr
	if (!MissileBodyMesh)
	{
		return false;
	}

	const FVector TargetLocation = GetTargetActor()->GetActorLocation();
	const FVector MissileConeLocation = MissileBodyMesh->GetSocketLocation("LockingCone");
	const FVector MissileToTarget = TargetLocation - MissileConeLocation;
	const float TargetDistanceSquared = MissileToTarget.Size();

	// First check if the target is within locking cone range.
	if (IsLockingConeRangeLimited())
	{
		const float MaximumRangeSquared = FMath::Pow(GetLockingConeMaximumRange(), 2.f);

		if (TargetDistanceSquared > MaximumRangeSquared)
		{
			return false;
		}
	}

	// Now check if we're within the locking cone angle.
	if (IsLockingConeAngleLimited())
	{
		// Angle = cos^-1(forward vector . vector to target)

		const FVector MissileToTargetNormalized = MissileToTarget.GetSafeNormal();
		const FVector ActorForwardVector = GetActorForwardVector();

		const float DotProduct = FVector::DotProduct(MissileToTargetNormalized, ActorForwardVector);
		const float Angle = FMath::Acos(DotProduct);

		if (Angle > GetLockingConeAngle())
		{
			return false;
		}
	}

	return true;
}

void ABasicMissile::SetTarget_Implementation(AActor* NewTargetActor)
{
	SetTargetActor(NewTargetActor);
}

bool ABasicMissile::GetTarget_Implementation(AActor*& OutTargetActor)
{
	OutTargetActor = GetTargetActor();

	return GetTargetActor() != nullptr;
}

void ABasicMissile::BeginPlay()
{
	Super::BeginPlay();

	// Initialize Current Locking Cone Target Check Cooldown as our maximum
	CurrentLockingConeTargetCheckCooldown = GetLockingConeTargetCheckCooldown();
}

void ABasicMissile::TickHomingMissileMovement(const float DeltaTime)
{
	if (!GetTargetActor())
	{
		return;
	}

	// Check proximity on our target first, as we may have moved "past" them during the last physics frame, which is part of the reason for the proximity fuse, as I can only check if I moved "through" or "into" the target.
	const FTransform TargetTransform = GetTargetActor()->GetActorTransform();
	const FVector MissileConeLocation = MissileBodyMesh->GetSocketLocation("LockingCone");
	const FVector MissileConeToTarget = TargetTransform.GetLocation() - MissileConeLocation;

	const float ProximityFuseDistanceSquared = FMath::Pow(GetMissileProximityFuseDistance(), 2.f);
	const float DistanceFromTargetSquared = MissileConeToTarget.SizeSquared();

	if (DistanceFromTargetSquared <= ProximityFuseDistanceSquared)
	{
		// Asplode
		SetState(EBasicMissileState::EBMS_Exploding);

		ExplodeMissile();
	}

	// Check if missile still has positive lock on target
	// A missile doesn't need to check whether it still has a target lock every fraction of a second - It can get by with just a couple of times a second
	CurrentLockingConeTargetCheckCooldown -= DeltaTime;
	if (CurrentLockingConeTargetCheckCooldown <= 0.f)
	{
		const bool bTargetIsInLockingCone = IsTargetInLockingCone();

		if (!bTargetIsInLockingCone)
		{
			// move to EBasicMissileState::EBMS_LostLock here
			return;
		}
	}

	// Calculate and Apply missile rotation
	const FRotator MaximumRotation = GetMissileRotationRate() * DeltaTime;
	const FRotator MissileRotationDeadzoneTemp = GetMissileRotationDeadzone();

	FRotator FinalRotationDelta = FRotator::ZeroRotator;

	const FVector TargetLocation = TargetTransform.GetLocation();
	const FVector MissileLocation = GetActorLocation();
	const FVector MissileForwards = GetActorForwardVector();
	const FVector MissileUp = GetActorUpVector();

	// We want the missile to apply its roll first, so that it can use its fins more effectively for pitch and yaw
	const FVector ClosestPointOnMissileForwardVectorToTarget = UKismetMathLibrary::FindClosestPointOnLine(TargetLocation, MissileLocation, MissileForwards);
	const FVector DesiredUpVector = (TargetLocation - ClosestPointOnMissileForwardVectorToTarget).GetSafeNormal();
	const float TempRoll = FVector::DotProduct(MissileUp, DesiredUpVector);
	float RollDelta = FMath::Acos(TempRoll);
	float RollDeltaDegrees = FMath::RadiansToDegrees(RollDelta);
	if (RollDeltaDegrees > MissileRotationDeadzoneTemp.Roll)
	{
		const FVector Cross = FVector::CrossProduct(MissileUp, DesiredUpVector);
		if (FVector::DotProduct(MissileForwards, Cross) > 0)
		{
			RollDeltaDegrees = -RollDeltaDegrees;
		}

		FinalRotationDelta.Roll = FMath::Clamp(RollDeltaDegrees, -MaximumRotation.Roll, MaximumRotation.Roll);
	}
	else
	{
		const FTransform MissileTransform = GetActorTransform();
		const FVector TargetLocationInMissileCoordinateSpace = MissileTransform.InverseTransformPosition(TargetLocation);
		const float DifferenceInLocationSizeSquared = TargetLocationInMissileCoordinateSpace.SizeSquared();

		// Pitch = asin(z diff / distance)
		const float DifferenceInLocationZSquared = FMath::Pow(TargetLocationInMissileCoordinateSpace.Z, 2.f);
		const float TempZ = DifferenceInLocationZSquared / DifferenceInLocationSizeSquared;
		const float PitchDelta = FMath::Asin(TempZ);
		const float PitchDeltaDegrees = FMath::RadiansToDegrees(PitchDelta) * FMath::Sign<float>(TargetLocationInMissileCoordinateSpace.Z);

		// Yaw = asin(y diff / distance)
		const float DifferenceInLocationYSquared = FMath::Pow(TargetLocationInMissileCoordinateSpace.Y, 2.f);
		const float TempY = DifferenceInLocationYSquared / DifferenceInLocationSizeSquared;
		const float YawDelta = FMath::Asin(TempY);
		const float YawDeltaDegrees = FMath::RadiansToDegrees(YawDelta) * FMath::Sign<float>(TargetLocationInMissileCoordinateSpace.Y);

		// apply pitch and yaw
		if (FMath::Abs(PitchDeltaDegrees) > MissileRotationDeadzoneTemp.Pitch)
		{
			FinalRotationDelta.Pitch = FMath::Clamp(PitchDeltaDegrees, -MaximumRotation.Pitch, MaximumRotation.Pitch);
		}
		if (FMath::Abs(YawDeltaDegrees) > MissileRotationDeadzoneTemp.Yaw)
		{
			FinalRotationDelta.Yaw = FMath::Clamp(YawDeltaDegrees, -MaximumRotation.Yaw, MaximumRotation.Yaw);
		}
	}

	AddActorLocalRotation(FinalRotationDelta);

	// Apply missile force
	const float MissileForceSize = GetCurrentEngineThrust();
	const FVector MissileThrusterForce = MissileForwards * MissileForceSize * DeltaTime;
	if(ProjectileMovementComponent)
	{ 
		ProjectileMovementComponent->AddForce(MissileThrusterForce);
	}

	// Let the PhysX magic happen
}

void ABasicMissile::TickThrusterAcceleration(const float DeltaTime)
{
	const float TargetEngineThrustPercentageTemp = GetTargetEngineThrustPercentage();
	const float CurrentEngineThrustPercentageTemp = GetCurrentEngineThrustPercentage();

	// check if our target thrust percentage differs from our current
	if (FMath::IsNearlyEqual(TargetEngineThrustPercentageTemp, CurrentEngineThrustPercentageTemp, FLT_EPSILON))
	{
		return;
	}

	const UCurveFloat* MissileForceRampUpCurveTemp = GetMissileForceRampUpCurve();
	if (!MissileForceRampUpCurveTemp)
	{
		return;
	}
	float MinMissileForce = 0.f;
	float MaxMissileForce = 0.f;
	MissileForceRampUpCurveTemp->GetValueRange(MinMissileForce, MaxMissileForce);

	// Figure out whether we need to move our time value backwards or forwards
	const float DeltaTimeAdjusted = TargetEngineThrustPercentageTemp < CurrentEngineThrustPercentageTemp ? -DeltaTime : DeltaTime;

	// find our new thruster force and store it
	const float CurrentEngineThrustPercentageAsTimeTemp = GetCurrentEngineThrustPercentageAsTime();
	const float NewCurrentEngineThrustPercentageAsTime = CurrentEngineThrustPercentageAsTimeTemp + DeltaTimeAdjusted;
	const float NewEngineThrust = MissileForceRampUpCurveTemp->GetFloatValue(NewCurrentEngineThrustPercentageAsTime);
	const float NewEngineThrustPercentage = NewEngineThrust / MaxMissileForce;

	SetCurrentEngineThrustPercentageAsTime(NewCurrentEngineThrustPercentageAsTime);
	SetCurrentEngineThrustPercentage(NewEngineThrustPercentage);
	SetCurrentEngineThrust(NewEngineThrust);

	// Time adjustment, because I can't access a time by float value and just run to the time, will run past my target here, so we need to check for that here, and "adjust" my target values to fit the one that we've decided to stop at.
	// Thanks Unreal Engine -.-
	const bool bTargetThrustReachedOrSurpassed = FMath::IsNearlyEqual(NewEngineThrustPercentage, TargetEngineThrustPercentageTemp, FLT_EPSILON) || 
		(NewEngineThrustPercentage < TargetEngineThrustPercentageTemp && TargetEngineThrustPercentageTemp < CurrentEngineThrustPercentageTemp) || (NewEngineThrustPercentage > TargetEngineThrustPercentageTemp && TargetEngineThrustPercentageTemp > CurrentEngineThrustPercentageTemp);

	if (bTargetThrustReachedOrSurpassed)
	{
		SetTargetEngineThrustPercentage(NewEngineThrustPercentage);
	}
}

void ABasicMissile::TickGravityScale(const float DeltaTime)
{
	// This is a candidate for justification of putting behaviour in a new projectile movement component, but I'm unsure at the time of writing whether that would remove my ability to finely control when it is modified based on state.

	// Calculate gravity percentage based on velocity and apply to projectile movement component.
	if (!ProjectileMovementComponent)
	{
		return;
	}

	const FVector MissileVelocity = ProjectileMovementComponent->Velocity;
	const float MissileVelocitySizeSquared = MissileVelocity.SizeSquared();
	const float MaximumVelocitySizeSquared = FMath::Pow(ProjectileMovementComponent->GetMaxSpeed(), 2.f);
	const float CurrentPercentageOfMaximumVelocity = MissileVelocitySizeSquared / MaximumVelocitySizeSquared;

	const float VelocityPercentageOfZeroGravity = GetMissileVelocityPercentageOfGravityNegation();

	const FVector2D InputRange = { 0.f, VelocityPercentageOfZeroGravity };
	const FVector2D OutputRange = { 0.f, 1.f };
	const float NewGravityScale = CurrentPercentageOfMaximumVelocity >= VelocityPercentageOfZeroGravity ? 0.f : FMath::GetMappedRangeValueClamped(InputRange, OutputRange, CurrentPercentageOfMaximumVelocity);

	ProjectileMovementComponent->ProjectileGravityScale = NewGravityScale;
}
