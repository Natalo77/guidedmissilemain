
#pragma once

#include "CoreMinimal.h"

#include "GuidedMissile/Missile/MissileBase.h"

#include "GuidedMissile/Interfaces/HomingMissileInterface.h"

#include "BasicMissile.generated.h"

UENUM(BlueprintType)
enum class EBasicMissileState : uint8
{
	EBMS_Invalid		UMETA(DisplayName = "Invalid"),			// Missile is in a non-reality state e.g. in an object pool
	EBMS_PreFire		UMETA(DisplayName = "PreFire"),			// Missile is in position, ready to be fired e.g. loaded in a missile launcher
	EBMS_InitialLaunch	UMETA(DisplayName = "InitialLaunch"),	// Missile has been ejected from the launcher, but has not initiated its thrust/engine yet
	EBMS_Homing			UMETA(DisplayName = "Homing"),			// Missile has a lock on target and is homing in.
	EBMS_LostLock		UMETA(DisplayName = "LostLock"),		// Missile is flying with thrust, but has lost its target lock
	EBMS_Exploding		UMETA(DisplayName = "Exploding"),		// Missile has, by means of target proximity or too long without a lock, triggered its detonation, and is in the process of exploding.
};

/**
 * Implementation of missile base
 * Simple, velocity tracking missile. Locks onto a target, fires & heads towards target's position + their velocity (leading the target)
 * Explodes when in suitable proximity of target, or after a period of time left without a target.
 */
UCLASS()
class GUIDEDMISSILE_API ABasicMissile : public AMissileBase, public IHomingMissileInterface
{
	GENERATED_BODY()

public:

	/** Default Constructor */
	ABasicMissile();

	/** Called every frame */
	virtual void Tick(float DeltaTime) override;

	/** Prepare the missile for launch, supplying the transform with which to launch from. Moves the missile to state: PreFire */
	virtual void PrepareMissileForLaunch(const FTransform& LaunchTransform) override;

	/** Launches the missile into its initial launch stage */
	virtual void FireMissile() override;

	/** Detonates the missile */
	virtual void ExplodeMissile() override;

	/** Turns on the missile thrust and begins homing behaviour towards its assigned target */
	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void StartHoming();

	/** Disable/Enable collision for the entire missile */
	UFUNCTION(BlueprintCallable, Category = "Utility")
	virtual void SetCollisionEnabled(const bool bEnabled);

	/** Is the target "in view" of the missile */
	UFUNCTION(BlueprintCallable, Category = "Utility")
	virtual bool IsTargetInLockingCone();

	/** Current state of missile */
	UFUNCTION(BlueprintCallable, Category = "State")
	EBasicMissileState GetState() const { return State; }

	/** Current state of missile */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetState(EBasicMissileState val) { State = val; }

	/** The current percentage of thrust we are accelerating the engine towards. */
	UFUNCTION(BlueprintCallable, Category = "State")
	float GetTargetEngineThrustPercentage() const { return TargetEngineThrustPercentage; }

	/** The current percentage of thrust we are accelerating the engine towards. */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetTargetEngineThrustPercentage(float val) { TargetEngineThrustPercentage = FMath::Clamp(val, 0.f, 1.f); }

	/** What is the current percentage of engine thrust being used. */
	UFUNCTION(BlueprintCallable, Category = "State")
	float GetCurrentEngineThrustPercentage() const { return CurrentEngineThrustPercentage; }

	/** What is the current percentage of engine thrust being used. */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetCurrentEngineThrustPercentage(float val) { CurrentEngineThrustPercentage = FMath::Clamp(val, 0.f, 1.f); }

	/** What is the current engine thrust force size */
	UFUNCTION(BlueprintCallable, Category = "State")
	float GetCurrentEngineThrust() const { return CurrentEngineThrust; }

	/** What is the current engine thrust force size */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetCurrentEngineThrust(float val) { CurrentEngineThrust = val; }

	///////////////////////////////////////////////////////
	/// IHomingMissileInterface

	/** Set the homing target of this homing missile */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Target")
	void SetTarget(AActor* NewTargetActor);
	virtual void SetTarget_Implementation(AActor* NewTargetActor) override;

	/** Passes out the target actor of this homing missile. Returns whether or not the target is valid */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Target")
	bool GetTarget(AActor*& OutTargetActor);
	virtual bool GetTarget_Implementation(AActor*& OutTargetActor) override;

	///////////////////////////////////////////////////////
	/// End IHomingMissileInterface

	/** The current target of this missile */
	UFUNCTION(BlueprintCallable, Category = "Target")
	AActor* GetTargetActor() const { return TargetActor; }

	/** The current target of this missile */
	UFUNCTION(BlueprintCallable, Category = "Target")
	void SetTargetActor(AActor* val) { TargetActor = val; }

	/** The force used for the initial launch of the missile (the ejection, pre thrust enable) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetInitialLaunchForceSize() const { return InitialLaunchForceSize; }

	/** The force used for the initial launch of the missile (the ejection, pre thrust enable) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetInitialLaunchForceSize(float val) { InitialLaunchForceSize = val; }

	/** Duration in seconds that the initial launch state lasts before switching the thrust on and moving to the homing state */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetInitialLaunchStageDuration() const { return InitialLaunchStageDuration; }

	/** Duration in seconds that the initial launch state lasts before switching the thrust on and moving to the homing state */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetInitialLaunchStageDuration(float val) { InitialLaunchStageDuration = val; }

	/** Is the locking cone of the missile angle limited (targets outside of the half angle are considered outside of the locking cone) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	bool IsLockingConeAngleLimited() const { return bLockingConeIsAngleLimited; }

	/** Is the locking cone of the missile angle limited (targets outside of the half angle are considered outside of the locking cone) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetLockingConeIsAngleLimited(bool val) { bLockingConeIsAngleLimited = val; }

	/** The angle of the cone, projected from the front of the missile, that is defined as the "locking cone", the cone within which the missile's target is deemed to be "visible" by the missile - 180.f denotes all round visibility */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetLockingConeAngle() const { return LockingConeAngle; }

	/** The angle of the cone, projected from the front of the missile, that is defined as the "locking cone", the cone within which the missile's target is deemed to be "visible" by the missile - 180.f denotes all round visibility */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetLockingConeAngle(float val) { LockingConeAngle = val; }
	
	/** Is the locking cone of the missile range limited (targets further away than LockingConeMaximumRange are considered outside of the locking cone) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	bool IsLockingConeRangeLimited() const { return bLockingConeIsRangeLimited; }

	/** Is the locking cone of the missile range limited (targets further away than LockingConeMaximumRange are considered outside of the locking cone) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetLockingConeIsRangeLimited(bool val) { bLockingConeIsRangeLimited = val; }

	/** The maximum distance from the missile, further than which targets are considred to be outside of the locking cone */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetLockingConeMaximumRange() const { return LockingConeMaximumRange; }

	/** The maximum distance from the missile, further than which targets are considred to be outside of the locking cone */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetLockingConeMaximumRange(float val) { LockingConeMaximumRange = val; }

	/** How quickly (per second) does the missile rotate on each axis */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	FRotator GetMissileRotationRate() const { return MissileRotationRate; }

	/** How quickly (per second) does the missile rotate on each axis */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMissileRotationRate(FRotator val) { MissileRotationRate = val; }

	/** What margin of error is considered acceptable for the missile when en route to its target */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	FRotator GetMissileRotationDeadzone() const { return MissileRotationDeadzone; }

	/** What margin of error is considered acceptable for the missile when en route to its target */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMissileRotationDeadzone(FRotator val) { MissileRotationDeadzone = val; }

	/** The curve of 0..MaximumMissileForce against time, used to "ramp up" the missiles engine */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	class UCurveFloat* GetMissileForceRampUpCurve() const { return MissileForceRampUpCurve; }

	/** The curve of 0..MaximumMissileForce against time, used to "ramp up" the missiles engine */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMissileForceRampUpCurve(class UCurveFloat* val) { MissileForceRampUpCurve = val; }

	/** At what percentage of the missile's maximum velocity (defined by projectile movement component), will the missile negate the affects of gravity through the lift of its fins (gravity will be linearly interpolated from 1..0 between 0..ThisValue) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetMissileVelocityPercentageOfGravityNegation() const { return MissileVelocityPercentageOfGravityNegation; }

	/** At what percentage of the missile's maximum velocity (defined by projectile movement component), will the missile negate the affects of gravity through the lift of its fins (gravity will be linearly interpolated from 1..0 between 0..ThisValue) */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMissileVelocityPercentageOfGravityNegation(float val) { MissileVelocityPercentageOfGravityNegation = val; }

	/** How much damage does this missile do */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetMissileDamage() const { return MissileDamage; }

	/** How much damage does this missile do */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMissileDamage(float val) { MissileDamage = val; }

	/** Within what radius will this missile apply its damage when it explodes */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetMissileDamageRange() const { return MissileDamageRange; }

	/** Within what radius will this missile apply its damage when it explodes */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMissileDamageRange(float val) { MissileDamageRange = val; }

	/** How close to the target will the missile's proximity fuse trigger and detonate the missile */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|AtRuntime")
	float GetMissileProximityFuseDistance() const { return MissileProximityFuseDistance; }

	/** How close to the target will the missile's proximity fuse trigger and detonate the missile */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|AtRuntime")
	void SetMissileProximityFuseDistance(float val) { MissileProximityFuseDistance = val; }

	/** How long (seconds) between each locking cone check (angle and range) while homing behaviour is active */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|Performance")
	float GetLockingConeTargetCheckCooldown() const { return LockingConeTargetCheckCooldown; }

	/** How long (seconds) between each locking cone check (angle and range) while homing behaviour is active */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|Performance")
	void SetLockingConeTargetCheckCooldown(float val) { LockingConeTargetCheckCooldown = val; }

protected:

	/** Called when play starts */
	virtual void BeginPlay() override;

	/** Performs tick behaviour for homing missile behaviour (rotating/applying force/etc.) */
	UFUNCTION(BlueprintCallable, Category = "Tick Functions")
	virtual void TickHomingMissileMovement(const float DeltaTime);

	/** Performs tick behaviour for accelerating/deccelerating the engine's thrust towards the target thrust */
	UFUNCTION(BlueprintCallable, Category = "Tick Functions")
	virtual void TickThrusterAcceleration(const float DeltaTime);

	/** Performs tick behaviour for modifying gravity (to simulate lift) based on velocity */
	UFUNCTION(BlueprintCallable, Category = "Tick Functions")
	virtual void TickGravityScale(const float DeltaTime);

	/** Mesh of the missile body (that which has collision) */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* MissileBodyMesh = nullptr;

	/** Mesh for a fin of the missile */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* MissileFinMesh1 = nullptr;

	/** Mesh for a fin of the missile */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* MissileFinMesh2 = nullptr;

	/** Mesh for a fin of the missile */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* MissileFinMesh3 = nullptr;

	/** Mesh for a fin of the missile */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* MissileFinMesh4 = nullptr;

	/** Movement component for this type of missile */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UProjectileMovementComponent* ProjectileMovementComponent = nullptr;

	/** The force used for the initial launch of the missile (the ejection, pre thrust enable) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	float InitialLaunchForceSize = 500.f;

	/** Duration in seconds that the initial launch state lasts before switching the thrust on and moving to the homing state */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	float InitialLaunchStageDuration = 1.f;

	/** Is the locking cone of the missile angle limited (targets outside of the half angle are considered outside of the locking cone) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	bool bLockingConeIsAngleLimited = true;
	
	/** The angle of the cone, projected from the front of the missile, that is defined as the "locking cone", the cone within which the missile's target is deemed to be "visible" by the missile */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable", meta = (ClampMin = 1.f, ClampMax = 179.f, EditCondition = "bLockingConeIsAngleLimited"))
	float LockingConeAngle = 90.f;

	/** Is the locking cone of the missile range limited (targets further away than LockingConeMaximumRange are considered outside of the locking cone) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	bool bLockingConeIsRangeLimited = false;
	
	/** The maximum distance from the missile, further than which targets are considered to be outside of the locking cone */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable", meta = (ClampMin = 50.f, EditCondition = "bLockingConeIsRangeLimited"))
	float LockingConeMaximumRange = 10000.f;

	/** How quickly (per second) does the missile rotate on each axis */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	FRotator MissileRotationRate = FRotator(.5f, .5f, 5.0f);

	/** What margin of error is considered acceptable for the missile when en route to its target */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	FRotator MissileRotationDeadzone = FRotator(0.f, 0.f, .01f);

	/** The curve of 0..MaximumMissileForce against time, used to "ramp up" the missiles engine */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	class UCurveFloat* MissileForceRampUpCurve = nullptr;

	/** At what percentage of the missile's maximum velocity (defined by projectile movement component), will the missile negate the affects of gravity through the lift of its fins (gravity will be linearly interpolated from 1..0 between 0..ThisValue) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable", meta = (ClampMin = 0.01f, ClampMax = 0.99f))
	float MissileVelocityPercentageOfGravityNegation = 0.5f;

	/** How much damage does this missile do */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	float MissileDamage = 100.f;

	/** Within what radius will this missile apply its damage when it explodes */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable")
	float MissileDamageRange = 500.f;

	/** How close to the target will the missile's proximity fuse trigger and detonate the missile */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tweakable|AtRuntime")
	float MissileProximityFuseDistance = 100.f;

	/** How long (seconds) between each locking cone check (angle and range) while homing behaviour is active */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tweakable|Performance", meta = (ClampMin = 0.f))
	float LockingConeTargetCheckCooldown = .5f;

	/** Current state of missile */
	UPROPERTY(BlueprintReadWrite, Category = "State")
	EBasicMissileState State = EBasicMissileState::EBMS_Invalid;

	/** The current target of this missile */
	UPROPERTY(BlueprintReadWrite, Category = "State")
	AActor* TargetActor = nullptr;

	/** The current percentage of thrust we are accelerating the engine towards. */
	UPROPERTY(BlueprintReadOnly, Category = "State")
	float TargetEngineThrustPercentage = 0.f;

	/** What is the current percentage of engine thrust being used. */
	UPROPERTY(BlueprintReadOnly, Category = "State")
	float CurrentEngineThrustPercentage = 0.f;

	/** What is the current engine thrust force size */
	UPROPERTY(BlueprintReadOnly, Category = "State")
	float CurrentEngineThrust = 0.f;

private:

	/** The current engine thrust percentage as it is located on the time axis of the MissileForceRampUpCurve */
	UFUNCTION(BlueprintCallable, Category = "State")
	float GetCurrentEngineThrustPercentageAsTime() const { return CurrentEngineThrustPercentageAsTime; }

	/** The current engine thrust percentage as it is located on the time axis of the MissileForceRampUpCurve */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetCurrentEngineThrustPercentageAsTime(float val) { CurrentEngineThrustPercentageAsTime = val; }

	/** Timer handle for the transition from FireMissile to StartHoming */
	FTimerHandle TimerHandle_BeginHomingAfterInitialLaunch;

	/** How long is left before the next locking cone check (angle and range) */
	UPROPERTY(BlueprintReadOnly, Category = "State", meta = (AllowPrivateAccess = "true"))
	float CurrentLockingConeTargetCheckCooldown = 0.f;

	/** The current engine thrust percentage as it is located on the time axis of the MissileForceRampUpCurve */
	UPROPERTY(BlueprintReadOnly, Category = "State", meta = (AllowPrivateAccess = "true"))
	float CurrentEngineThrustPercentageAsTime = 0.f;
	
};
