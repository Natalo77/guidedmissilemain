#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "MissileBase.generated.h"

DECLARE_DYNAMIC_DELEGATE_RetVal_OneParam(bool, FOnMissileExploded, AMissileBase*, Missile);

/**
 * Abstract missile class - Defines methods and properties all missile types will need.
 */
UCLASS()
class GUIDEDMISSILE_API AMissileBase : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Default Constructor */
	AMissileBase();

	/** Called every frame */
	virtual void Tick(float DeltaTime) override;

	// This will likely need an option for no specified transform, to fit the case where we've already attached the missile to its launch location ahead of time.
	/** Prepare the missile for launch, supplying the transform with which to launch from. */
	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void PrepareMissileForLaunch(const FTransform& LaunchTransform);

	/** Fires the missile */
	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void FireMissile();

	/** Detonates the missile */
	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void ExplodeMissile();

	/** Who is responsible for this missile being fired */
	UFUNCTION(BlueprintCallable, Category = "State")
	class AController* GetMissileInstigatorController() const { return MissileInstigatorController; }

	/** Who is responsible for this missile being fired */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetMissileInstigatorController(class AController* val) { MissileInstigatorController = val; }

	/** Called when the missile has exploded */
	UPROPERTY()
	FOnMissileExploded OnMissileExploded;

protected:
	
	/** Called when play starts */
	virtual void BeginPlay() override;

	/** Called when the missile is fired */
	UFUNCTION(BlueprintNativeEvent, Category = "Events")
	void OnMissileFired();
	virtual void OnMissileFired_Implementation();

	/** Called when the missile explodes */
	UFUNCTION(BlueprintNativeEvent, Category = "Events")
	void OnMissileExplode();
	virtual void OnMissileExplode_Implementation();

	/** Who is responsible for this missile being fired */
	UPROPERTY(BlueprintReadOnly, Category = "State")
	class AController* MissileInstigatorController = nullptr;
};
