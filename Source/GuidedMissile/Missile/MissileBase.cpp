
#include "GuidedMissile/Missile/MissileBase.h"

AMissileBase::AMissileBase()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AMissileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMissileBase::PrepareMissileForLaunch(const FTransform& LaunchTransform)
{

}

void AMissileBase::FireMissile()
{
	OnMissileFired();
}

void AMissileBase::ExplodeMissile()
{
	OnMissileExploded.Execute(this);

	OnMissileExplode();
}

void AMissileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMissileBase::OnMissileFired_Implementation()
{

}

void AMissileBase::OnMissileExplode_Implementation()
{

}
