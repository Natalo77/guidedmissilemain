
#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"

#include "GuidedMissile/Interfaces/CanLaunchMissilesInterface.h"

#include "GuidedMissileCharacter.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogGuidedMissileCharacter, Log, All)

UCLASS(config=Game)
class AGuidedMissileCharacter : public ACharacter, public ICanLaunchMissilesInterface
{
	GENERATED_BODY()

public:

	/** Default Constructor */
	AGuidedMissileCharacter();

	/** Check the currently hovered enemy, and fire a missile at them if possible */
	UFUNCTION(BlueprintCallable, Category = "Actions|Missile")
	virtual void FireMissile();

	/** The maximum distance from an enemy we can be and still be able to fire a missile at it */
	UFUNCTION(BlueprintCallable, Category = "Actions|Missile")
	virtual float GetMaximumMissileLockOnDistance() const { return MaximumMissileLockOnDistance; }

	/** the Z & Y components of the box trace used to lock on before firing a missile (how much "give" does the lock on system allow for) */
	UFUNCTION(BlueprintCallable, Category = "Actions|Missile")
	virtual float GetMissileLockOnTraceBoxHalfSize() const { return MissileLockOnTraceBoxHalfSize; }

	/** What object types can we fire a missile at */
	UFUNCTION(BlueprintCallable, Category = "Actions|Missile")
	virtual TArray<TEnumAsByte<EObjectTypeQuery>> GetMissileLockOnObjectTypes() const { return MissileLockOnObjectTypes; }

	///////////////////////////////////////////////////
	/// ICanLaunchMissilesInterface

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Launch")
	int32 GetNumberOfLaunchLocations();
	virtual int32 GetNumberOfLaunchLocations_Implementation() override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Launch")
	bool GetTransformForLaunchLocation(const int32 LaunchLocationNumber, FTransform& OutLaunchTransform);
	virtual bool GetTransformForLaunchLocation_Implementation(const int32 LaunchLocationNumber, FTransform& OutLaunchTransform) override;

	/** Get the desired cooldown duration for the specified launch location index (starting from 1) */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Launch")
	bool GetCooldownForLaunchLocation(const int32 LaunchLocationNunber, float& OutCooldown);
	virtual bool GetCooldownForLaunchLocation_Implementation(const int32 LaunchLocationNunber, float& OutCooldown) override;

	///////////////////////////////////////////////////
	/// End ICanLaunchMissilesInterface

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	/** The minimum duration between missile fires */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|Missile")
	float GetMissileLaunchCooldownDuration() const { return MissileLaunchCooldownDuration; }

	/** The minimum duration between missile fires */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|Missile")
	void SetMissileLaunchCooldownDuration(float val) { MissileLaunchCooldownDuration = val; }

	/** The vertical offset to apply to the character's head when determining where to spawn missiles */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|Missile")
	float GetMissileSpawnLocationVerticalOffset() const { return MissileSpawnLocationVerticalOffset; }

	/** The vertical offset to apply to the character's head when determining where to spawn missiles */
	UFUNCTION(BlueprintCallable, Category = "Tweakable|Missile")
	void SetMissileSpawnLocationVerticalOffset(float val) { MissileSpawnLocationVerticalOffset = val; }

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components|Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom = nullptr;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components|Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components|Behaviour", meta = (AllowPrivateAccess = "true"))
	class UMissileComponent* MissileComponent = nullptr;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	
protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Called to bind input to functionality */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** The furthest distance away from a target we can be and still be able to fire a missile at it. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Actions|Missile")
	float MaximumMissileLockOnDistance = 1000.f;

	/** How big the Z & Y components of the box trace used to lock on before firing a missile (how much "give" does the lock on system allow for) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Actions|Missile")
	float MissileLockOnTraceBoxHalfSize = 50.f;

	/** What object types can we fire a missile at */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Actions|Missile")
	TArray<TEnumAsByte<EObjectTypeQuery>> MissileLockOnObjectTypes = {};

	/** The minimum duration between missile fires */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tweakable|Missile")
	float MissileLaunchCooldownDuration = 1.f;

	/** The vertical offset to apply to the character's head when determining where to spawn missiles */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tweakable|Missile")
	float MissileSpawnLocationVerticalOffset = 10.f;
};

