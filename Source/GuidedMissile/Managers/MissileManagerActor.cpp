
#include "GuidedMissile/Managers/MissileManagerActor.h"

#include "GuidedMissile/Missile/MissileBase.h"
#include "GuidedMissile/Missile/BasicMissile/BasicMissile.h"

#include <Engine/World.h>

AMissileManagerActor::AMissileManagerActor()
{
	// Manager doesn't need to tick, as we'll only ever perform static actions with it.
	PrimaryActorTick.bCanEverTick = false;
}

AMissileBase* AMissileManagerActor::GetMissile(const EMissileType MissileType)
{
	if (!GetWorld())
	{
		return nullptr;
	}

	// Default implementation is just to create a missile and give it to the caller. This should be using pooling in a future iteration.
	AMissileBase* Missile = nullptr;
	TSubclassOf<AMissileBase> MissileClass = AMissileBase::StaticClass();
	const bool bMissileTypeHasValidClass = GetMissileClassForType(MissileType, MissileClass);
	if (!bMissileTypeHasValidClass)
	{
		return nullptr;
	}

	const FTransform SpawnTransform = FTransform(FRotator::ZeroRotator, FVector(0.f, 0.f, GetWorld()->GetWorldSettings()->KillZ + 100.f));
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Missile = GetWorld()->SpawnActor<ABasicMissile>(MissileClass, SpawnTransform, SpawnParams);

	return Missile;
}

bool AMissileManagerActor::ReturnMissile(AMissileBase* Missile)
{
	if (!Missile)
	{
		return true;
	}

	Missile->Destroy();

	return true;
}

void AMissileManagerActor::BeginPlay()
{
	Super::BeginPlay();

	// I *could* perform a check for duplicates within the world here, but it's such a small project that I don't think it's necessary
}

const bool AMissileManagerActor::GetMissileClassForType(const EMissileType MissileType, TSubclassOf<AMissileBase>& OutMissileClass)
{
	TSubclassOf<AMissileBase>* MissileClassPtr = MissileTypeClasses.Find(MissileType);
	if (MissileClassPtr)
	{
		OutMissileClass = *MissileClassPtr;
		return true;
	}

	return false;
}
