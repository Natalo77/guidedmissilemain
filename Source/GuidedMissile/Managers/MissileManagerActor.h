
#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "GuidedMissile/Types/GuidedMissileTypes.h"

#include "MissileManagerActor.generated.h"

/*
* Class to manage and pool missile instead of having them spawned on demand
*/
UCLASS()
class GUIDEDMISSILE_API AMissileManagerActor : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Default Constructor */
	AMissileManagerActor();

	/** Get a new missile, ready for use */
	UFUNCTION(BlueprintCallable, Category = "Missile")
	virtual class AMissileBase* GetMissile(const EMissileType MissileType);

	/** Return a used missile back to the pooling system, returning whether the operation was successful */
	UFUNCTION(BlueprintCallable, Category = "Missile")
	virtual bool ReturnMissile(class AMissileBase* Missile);

protected:

	/** Called when play starts */
	virtual void BeginPlay() override;

	/** Passes out the concrete class for the passed in missile type, returns whether there is a valid class for the type specified */
	UFUNCTION(BlueprintCallable, Category = "Missile")
	const bool GetMissileClassForType(const EMissileType MissileType, TSubclassOf<AMissileBase>& OutMissileClass);

	/** Maps missile types to their respective classes */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Tweakable")
	TMap<EMissileType, TSubclassOf<class AMissileBase>> MissileTypeClasses = {};

};
