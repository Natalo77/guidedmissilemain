
#pragma once

#include "CoreMinimal.h"

/**
 * Enumerates all missile types in this project
 */
UENUM(BlueprintType)
enum class EMissileType : uint8
{
	EMT_Invalid			UMETA(DisplayName = "Invalid"),			// Invalid Data
	EMT_BasicMissile	UMETA(DisplayName = "BasicMissile"),	// Basic, Velocity Tracking Missile
};