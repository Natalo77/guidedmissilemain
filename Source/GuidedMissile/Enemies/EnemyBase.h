
#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Pawn.h"

#include "EnemyBase.generated.h"

/*
* Abstract enemy pawn for use in demonstration.
*/
UCLASS()
class GUIDEDMISSILE_API AEnemyBase : public APawn
{
	GENERATED_BODY()

public:
	
	/** Default Constructor */
	AEnemyBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	
	/** Called when play starts */
	virtual void BeginPlay() override;

	/** Called to bind input to functionality */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
