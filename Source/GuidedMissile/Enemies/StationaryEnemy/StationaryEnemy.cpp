
#include "GuidedMissile/Enemies/StationaryEnemy/StationaryEnemy.h"

AStationaryEnemy::AStationaryEnemy()
{
	// Create proxy root
	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	check(RootSceneComponent);
	SetRootComponent(RootSceneComponent);

	// Create body mesh and attach to root
	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyMesh"));
	check(BodyMesh);
	BodyMesh->SetupAttachment(GetRootComponent());
}

float AStationaryEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	const float OldHealth = GetCurrentHealth();

	const float NewHealth = OldHealth - Damage;

	SetCurrentHealth(NewHealth);

	if (NewHealth <= 0.f)
	{
		Die();
	}

	return Damage;
}

void AStationaryEnemy::Die()
{
	this->Destroy();
}
