
#pragma once

#include "CoreMinimal.h"

#include "GuidedMissile/Enemies/EnemyBase.h"

#include "StationaryEnemy.generated.h"

/**
 * Simple implementation of enemy base - stands stationary and waits to be hit by a missile.
 */
UCLASS()
class GUIDEDMISSILE_API AStationaryEnemy : public AEnemyBase
{
	GENERATED_BODY()

public:

	/** Default Constructor */
	AStationaryEnemy();

	/** Take Damage */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	/** Kill this actor */
	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void Die();

	/** What is the maximum health this enemy can have */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	float GetMaximumHealth() const { return MaximumHealth; }

	/** What is the maximum health this enemy can have */
	UFUNCTION(BlueprintCallable, Category = "Tweakable")
	void SetMaximumHealth(float val) { MaximumHealth = val; }

	/** What is the current health of this enemy */
	UFUNCTION(BlueprintCallable, Category = "State")
	float GetCurrentHealth() const { return CurrentHealth; }

	/** What is the current health of this enemy */
	UFUNCTION(BlueprintCallable, Category = "State")
	void SetCurrentHealth(float val) { CurrentHealth = val; }

protected:

	/** Proxy scene root for enemy */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class USceneComponent* RootSceneComponent = nullptr;

	/** Mesh for the body of the enemy */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* BodyMesh = nullptr;

	/** What is the maximum health this enemy can have */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tweakable")
	float MaximumHealth = 100.f;

	/** What is the current health of this enemy */
	UPROPERTY(BlueprintReadOnly, Category = "State")
	float CurrentHealth = MaximumHealth;
	
};
