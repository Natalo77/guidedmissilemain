
#include "GuidedMissileCharacter.h"

#include "Camera/CameraComponent.h"

#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include <Kismet/KismetSystemLibrary.h>

#include "GuidedMissile/Components/MissileComponent.h"

#include "GuidedMissile/Types/GuidedMissileTypes.h"

DEFINE_LOG_CATEGORY(LogGuidedMissileCharacter);

AGuidedMissileCharacter::AGuidedMissileCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	// Create missile behaviour component
	MissileComponent = CreateDefaultSubobject<UMissileComponent>(TEXT("MissileComponent"));
	check(MissileComponent);
}

void AGuidedMissileCharacter::FireMissile()
{
	UE_LOG(LogGuidedMissileCharacter, Log, TEXT("FireMissile"));

	// Method: Get Camera, Get forward vector, trace box along path, if first hit is enemy, fire missile.

	// Get Player View Point
	const APlayerController* const PlayerController = GetController<APlayerController>();
	if (!PlayerController)
	{
		return;
	}
	FVector PlayerViewPointLoc = FVector::ZeroVector;
	FRotator PlayerViewPointRot = FRotator::ZeroRotator;
	PlayerController->GetPlayerViewPoint(PlayerViewPointLoc, PlayerViewPointRot);

	// Construct trace parameters and fire trace
	const FVector MissileLockOnTraceEndLoc = PlayerViewPointLoc + PlayerViewPointRot.Vector() * GetMaximumMissileLockOnDistance();
	const float TempMissileLockOntraceBoxHalfSize = GetMissileLockOnTraceBoxHalfSize();
	const FVector MissileLockOnTraceHalfSize = FVector(1.f, TempMissileLockOntraceBoxHalfSize, TempMissileLockOntraceBoxHalfSize);

	FHitResult HitResult;
	UKismetSystemLibrary::BoxTraceSingleForObjects(this, PlayerViewPointLoc, MissileLockOnTraceEndLoc, MissileLockOnTraceHalfSize, PlayerViewPointRot, GetMissileLockOnObjectTypes(), false, {}, EDrawDebugTrace::ForDuration, HitResult, true);

	// Fire a missile with the given target. Just use a basic missile for now. Future iterations will pass in the actual type e.g. javelin, dummy, basic etc.
	if (HitResult.bBlockingHit)
	{
		AActor* HitActor = HitResult.GetActor();
		if (!HitActor || !MissileComponent)
		{
			return;
		}
		UE_LOG(LogGuidedMissileCharacter, Log, TEXT("FireMissile At %s"), *HitActor->GetHumanReadableName());
		MissileComponent->FireMissileAt(HitActor, EMissileType::EMT_BasicMissile);
	}
}

int32 AGuidedMissileCharacter::GetNumberOfLaunchLocations_Implementation()
{
	return 1;
}

bool AGuidedMissileCharacter::GetTransformForLaunchLocation_Implementation(const int32 LaunchLocationNumber, FTransform& OutLaunchTransform)
{
	
	OutLaunchTransform = FTransform(FRotator::ZeroRotator, FVector::ZeroVector);

	// We only have one launch location, and it's above the head.
	// This will be better done as a switch when I need more launch locations
	if (LaunchLocationNumber == 1)
	{
		FVector LaunchLocation = GetCapsuleComponent()->GetComponentLocation();
		LaunchLocation.Z += GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 2.f;
		OutLaunchTransform.SetLocation(LaunchLocation);

		// Angle the missile up by 45 degrees.
		FRotator LaunchRotation = GetCapsuleComponent()->GetForwardVector().Rotation();
		LaunchRotation.Pitch += 45.f;
		OutLaunchTransform.SetRotation(LaunchRotation.Quaternion());

		return true;
	}

	return false;
}

bool AGuidedMissileCharacter::GetCooldownForLaunchLocation_Implementation(const int32 LaunchLocationNunber, float& OutCooldown)
{
	OutCooldown = 0.f;

	// We only have one launch location for now, above the heads.
	// This will be better done as a switch when we have more launch locations, potentially even turning the launch locations into an interface themselves
	if (LaunchLocationNunber == 1)
	{
		OutCooldown = GetMissileLaunchCooldownDuration();

		return true;
	}

	return false;
}

void AGuidedMissileCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGuidedMissileCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGuidedMissileCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGuidedMissileCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGuidedMissileCharacter::LookUpAtRate);

	// Bind input for firing missiles etc.
	PlayerInputComponent->BindAction("FireMissile", EInputEvent::IE_Released, this, &AGuidedMissileCharacter::FireMissile);
}

void AGuidedMissileCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AGuidedMissileCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AGuidedMissileCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AGuidedMissileCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
