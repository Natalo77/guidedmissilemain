
#include "GuidedMissile/Components/MissileComponent.h"

#include "GuidedMissile/Missile/MissileBase.h"

#include "GuidedMissile/Managers/MissileManagerActor.h"

#include <Kismet/GameplayStatics.h>

#include "GuidedMissile/Interfaces/CanLaunchMissilesInterface.h"
#include "GuidedMissile/Interfaces/HomingMissileInterface.h"


UMissileComponent::UMissileComponent()
{
	// I don't *think* we'll need to tick this component, but this is subject to change
	PrimaryComponentTick.bCanEverTick = true;
}

void UMissileComponent::FireMissileAt(AActor* Actor, const EMissileType MissileType)
{
	if (!Actor)
	{
		return;
	}

	if (!MissileManager)
	{
		return;
	}

	// Obtain and set up returning of a missile
	AMissileBase* Missile = MissileManager->GetMissile(MissileType);

	if (!Missile)
	{
		return;
	}

	Missile->OnMissileExploded.BindUFunction(MissileManager, "ReturnMissile");

	// Setup and move my missile to the correct location here.
	// This has a high likelihood of needing to be split up into separate parts, where missiles are "reloaded" and attached to the launching object, then we just choose the launch location (containing a rocket) to fire here.
	FTransform LaunchTransform = {};
	int32 LaunchLocationNumberUsed = 0;
	const bool bFreeLaunchLocationExists = GetFreeMissileLaunchLocation(LaunchTransform, LaunchLocationNumberUsed);
	if (!bFreeLaunchLocationExists)
	{
		// We need to queue this instead of just returning, so that when a launch location becomes available, we can load and fire the queued missile.
		return;
	}

	// Prepare the missile for launch and fire it immediately - As stated above, we'll probably want to prepare them beforehand in future iterations of the mechanic
	Missile->PrepareMissileForLaunch(LaunchTransform);

	// Pass in the instigating controller
	APawn* OwningObject_Pawn = Cast<APawn>(OwningObject);
	if (OwningObject_Pawn)
	{
		Missile->SetMissileInstigatorController(OwningObject_Pawn->GetController());
	}

	// Give the missile its target here, if it accepts one.
	if(Missile->GetClass()->ImplementsInterface(UHomingMissileInterface::StaticClass()))
	{
		IHomingMissileInterface::Execute_SetTarget(Missile, Actor);
	}

	Missile->FireMissile();

	// Begin the cool down for our management of the launch location used becoming "available" again.
	BeginLaunchLocationCooldownForLocationNumber(LaunchLocationNumberUsed);

}

bool UMissileComponent::GetFreeMissileLaunchLocation(FTransform& OutLaunchLocation, int32& LaunchLocationNumberUsed)
{
	OutLaunchLocation = {};
	LaunchLocationNumberUsed = 0;

	// For now, we'll just launch the missile from above the player's head. This will need a better queueing system, so that missiles can be queue fired if there are more locks than total firing locations.
	// I should also set up a more sophisticated system for being able to define a number of launch locations, determining whether they are free, and getting their transforms. I can improve the existing system here.

	int32 NextFreeLaunchLocationNumber = 0;
	const bool bFreeMissileLaunchLocationIsAvailable = GetNextFreeLaunchLocationNumber(NextFreeLaunchLocationNumber);
	if (!bFreeMissileLaunchLocationIsAvailable || !OwningObject)
	{
		return false;
	}

	const bool bTransformAcquiredSuccessfully = ICanLaunchMissilesInterface::Execute_GetTransformForLaunchLocation(OwningObject, NextFreeLaunchLocationNumber, OutLaunchLocation);
	if (bTransformAcquiredSuccessfully)
	{
		LaunchLocationNumberUsed = NextFreeLaunchLocationNumber;
	}

	return bTransformAcquiredSuccessfully;
}

void UMissileComponent::BeginPlay()
{
	Super::BeginPlay();	

	// Get reference to owning object that we're launching the missiles from
	if (GetOwner()->GetClass()->ImplementsInterface(UCanLaunchMissilesInterface::StaticClass()))
	{
		OwningObject = GetOwner();
	}
	check(OwningObject);

	// Initialize our own management of available locations from which to launch missiles.
	SetTotalLaunchLocations(ICanLaunchMissilesInterface::Execute_GetNumberOfLaunchLocations(OwningObject));
	GetAvailableLaunchLocations().Empty();
	for (int32 LaunchLocation = 1; LaunchLocation <= GetTotalLaunchLocations(); ++LaunchLocation)
	{
		GetAvailableLaunchLocations().Add(LaunchLocation);
	}

	// Get reference to missile manager singleton
	MissileManager = Cast<AMissileManagerActor>(UGameplayStatics::GetActorOfClass(this, AMissileManagerActor::StaticClass()));
	check(MissileManager);
}

bool UMissileComponent::GetNextFreeLaunchLocationNumber(int32& OutFreeLaunchLocation)
{
	OutFreeLaunchLocation = 0;

	if (GetAvailableLaunchLocations().Num() > 0)
	{
		OutFreeLaunchLocation = GetAvailableLaunchLocations()[0];
		GetAvailableLaunchLocations().RemoveAt(0);
		return true;
	}

	return false;
}

void UMissileComponent::FreeUpLaunchLocationNumber(int32 LaunchLocationNumber)
{
	const bool bLaunchLocationNumberIsValid = LaunchLocationNumber > 0 && LaunchLocationNumber <= GetTotalLaunchLocations() && !GetAvailableLaunchLocations().Contains(LaunchLocationNumber);
	if (!bLaunchLocationNumberIsValid)
	{
		return;
	}

	AvailableLaunchLocations.Add(LaunchLocationNumber);

	// The potential queueing system can call a function here like "OnLaunchLocationBecameAvailable" that checks for queued missiles and fires them.
}

bool UMissileComponent::BeginLaunchLocationCooldownForLocationNumber(const int32 LaunchLocationNumber)
{
	// Make sure we don't start a cooldown for an invalid launch location
	const bool bLaunchLocationNumberIsValid = LaunchLocationNumber > 0 && LaunchLocationNumber <= GetTotalLaunchLocations() && !GetAvailableLaunchLocations().Contains(LaunchLocationNumber);
	if (!bLaunchLocationNumberIsValid || !OwningObject)
	{
		return false;
	}

	// Query our owning launcher person thing for the cooldown duration for the specified launch location
	float CooldownDuration = 0.f;
	const bool bCooldownDurationAcquiredSuccessfully = ICanLaunchMissilesInterface::Execute_GetCooldownForLaunchLocation(OwningObject, LaunchLocationNumber, CooldownDuration);
	if (!bCooldownDurationAcquiredSuccessfully)
	{
		return false;
	}

	// Start the cooldown timer for the specified launch location
	FTimerHandle TimerHandle_LaunchLocationCooldownExpired;
	FTimerDelegate TimerDelegate_LaunchLocationCooldownExpired = FTimerDelegate::CreateUFunction(this, "FreeUpLaunchLocationNumber", LaunchLocationNumber);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_LaunchLocationCooldownExpired, TimerDelegate_LaunchLocationCooldownExpired, CooldownDuration, false);

	return true;
}
