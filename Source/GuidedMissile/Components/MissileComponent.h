
#pragma once

#include "CoreMinimal.h"

#include "Components/ActorComponent.h"

#include "GuidedMissile/Types/GuidedMissileTypes.h"

#include "MissileComponent.generated.h"

/*
* Component providing encapsulated missile firing capabilities & behaviour
*/
UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GUIDEDMISSILE_API UMissileComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	/** Default Constructor */
	UMissileComponent();

	/** Fire a missile at this actor */
	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void FireMissileAt(AActor* Actor, const EMissileType MissileType);

	/** Get a free location from which this missile can be fired, returning whether a free location was found or not */
	UFUNCTION(BlueprintCallable, Category = "Utility")
	virtual bool GetFreeMissileLaunchLocation(FTransform& OutLaunchLocation, int32& LaunchLocationNumberUsed);

	/** The number of launch locations available (which exist on the owning character) to this missile component */
	UFUNCTION(BlueprintCallable, Category = "Data")
	int32 GetTotalLaunchLocations() const { return TotalLaunchLocations; }

	/** The number of launch locations available (which exist on the owning character) to this missile component */
	UFUNCTION(BlueprintCallable, Category = "Data")
	void SetTotalLaunchLocations(int32 val) { TotalLaunchLocations = val; }

	/** The list of launch locations that we can use at the current time (updated so that the 0th element is always the next launch location we should use) */
	UFUNCTION(BlueprintCallable, Category = "State")
	TArray<int32>& GetAvailableLaunchLocations() { return AvailableLaunchLocations; }

protected:
	
	/** Called when play starts */
	virtual void BeginPlay() override;

	/** Passes out the next launch location we can use on the owning launcher object. Returns whether there was a free launch location or not */
	UFUNCTION(BlueprintCallable, Category = "Launch")
	bool GetNextFreeLaunchLocationNumber(int32& OutFreeLaunchLocation);

	/** Marks the specified launch location number as ready to use again */
	UFUNCTION(BlueprintCallable, Category = "State")
	void FreeUpLaunchLocationNumber(int32 LaunchLocationNumber);

	/** Begin the cooldown for our management of launch locations, so that we may use it again in the future. */
	UFUNCTION(BlueprintCallable, Category = "Launch")
	bool BeginLaunchLocationCooldownForLocationNumber(const int32 LaunchLocationNumber);

	/** Reference to owning object (that we're launching the missiles from), Guaranteed to inherit ICanLaunchMissilesInterface */
	UPROPERTY(BlueprintReadOnly, Category = "References")
	AActor* OwningObject = nullptr;
	
	/** Reference to world's missile manager */
	UPROPERTY(BlueprintReadOnly, Category = "References")
	class AMissileManagerActor* MissileManager = nullptr;

	/** The total number of launch locations (which exist on the owning character) that this missile component can use */
	UPROPERTY(BlueprintReadWrite, Category = "Data")
	int32 TotalLaunchLocations = -1;

	/** The list of launch locations that we can use at the current time (updated so that the 0th element is always the next launch location we should use) */
	UPROPERTY(BlueprintReadWrite, Category = "State")
	TArray<int32> AvailableLaunchLocations = {};
		
};
