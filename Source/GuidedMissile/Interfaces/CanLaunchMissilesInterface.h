
#pragma once

#include "CoreMinimal.h"

#include "UObject/Interface.h"

#include "CanLaunchMissilesInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCanLaunchMissilesInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Defines a set of operations that allow a class to launch missiles
 * A valid can launch missiles implementing class must have GetNumberOfLaunchLocations >= 1, otherwise there's nowhere to launch from.
 * - The launch locations are numbered, not indexed, so start from 1, not 0.
 */
class GUIDEDMISSILE_API ICanLaunchMissilesInterface
{
	GENERATED_BODY()

public:

	/** Get the number of launch locations (starting from 1) */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Launch")
	int32 GetNumberOfLaunchLocations();

	/** Get the transform used for a missile launching from the specified launch location index (starting from 1) */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Launch")
	bool GetTransformForLaunchLocation(const int32 LaunchLocationNumber, FTransform& OutLaunchTransform);

	/** Get the desired cooldown duration for the specified launch location index (starting from 1) */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Launch")
	bool GetCooldownForLaunchLocation(const int32 LaunchLocationNunber, float& OutCooldown);
};
