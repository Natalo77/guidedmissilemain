
#pragma once

#include "CoreMinimal.h"

#include "UObject/Interface.h"

#include "HomingMissileInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UHomingMissileInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Defines a set of operations that declares a missile to be "homing", in the sense that it has homing missile capabilities (heat seek, IR tracking etc.)
 */
class GUIDEDMISSILE_API IHomingMissileInterface
{
	GENERATED_BODY()

public:

	/** Set the homing target of this homing missile */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Target")
	void SetTarget(AActor* NewTargetActor);

	/** Passes out the target actor of this homing missile. Returns whether or not the target is valid */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Target")
	bool GetTarget(AActor*& OutTargetActor);
};
