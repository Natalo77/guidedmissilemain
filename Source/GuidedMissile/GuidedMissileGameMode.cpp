// Copyright Epic Games, Inc. All Rights Reserved.

#include "GuidedMissileGameMode.h"
#include "GuidedMissileCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGuidedMissileGameMode::AGuidedMissileGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
