// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GuidedMissileGameMode.generated.h"

UCLASS(minimalapi)
class AGuidedMissileGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGuidedMissileGameMode();
};



